package jp.alhinc.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CalculateSales {

	// 支店定義ファイル名
	private static final String FILE_NAME_BRANCH_LST = "branch.lst";

	// 支店別集計ファイル名
	private static final String FILE_NAME_BRANCH_OUT = "branch.out";

	// 商品定義ファイル名
	private static final String FILE_NAME_COMMODITY_LST = "commodity.lst";

	// 商品別集計ファイル名
	private static final String FILE_NAME_COMMODITY_OUT = "commodity.out";

	// エラーメッセージ
	private static final String UNKNOWN_ERROR = "予期せぬエラーが発生しました";
	private static final String FILE_NOT_EXIST = "定義ファイルが存在しません";
	private static final String FILE_INVALID_FORMAT = "定義ファイルのフォーマットが不正です";
	private static final String FILE_NOT_SERIAL_NUMBER = "売上ファイルが連番になっていません";
	private static final String FILE_TOTAL_AMOUNT_OVER = "合計金額が10桁を超えました";
	private static final String FILE_BRANCH_NAME_ERROR = "の支店コードが不正です";
	private static final String FILE_FORMAT_ERROR = "のフォーマットが不正です";

	/**
	 * メインメソッド
	 *
	 * @param コマンドライン引数
	 */
	public static void main(String[] args) {
		// 支店コードと支店名を保持するMap
		Map<String, String> branchNames = new HashMap<>();
		// 支店コードと売上金額を保持するMap
		Map<String, Long> branchSales = new HashMap<>();
		// 商品コードと商品名を保持するMap
		Map<String, String> commodityNames = new HashMap<>();
		// 商品コードと売上金額を保持するMap
		Map<String, Long> commoditySales = new HashMap<>();
		String branch = "支店";
		String commodity = "商品";
		String branchRule = "^[0-9]{3}$";
		String commodityRule = "^[0-9A-Za-z]{8}$";

		if (args.length != 1) {
			System.out.println(UNKNOWN_ERROR);
			return;
		}

		// 支店定義ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_BRANCH_LST, branchNames, branchSales, branchRule, branch)) {
			return;
		}

		// 商品別集計ファイル読み込み処理
		if (!readFile(args[0], FILE_NAME_COMMODITY_LST, commodityNames, commoditySales, commodityRule, commodity)) {
			return;
		}

		// ※ここから集計処理を作成してください。(処理内容2-1、2-2)
		File[] files = new File(args[0]).listFiles();
		List<File> rcdFiles = new ArrayList<>();
		List<String> fileCheck = new ArrayList<>();

		//000001等の売り上げファイルを選別しリスト「rcdFiles」に格納
		for (int i = 0; i < files.length; i++) {
			String fileName = files[i].getName();
			if (files[i].isFile() && (fileName.matches("^[0-9]{8}\\.rcd$"))) {
				rcdFiles.add(files[i]);
				fileCheck.add(fileName);
			}
		}

		//売上ファイルの名前が連番になっているかの確認処理
		Collections.sort(fileCheck);
		for (int i = 0; i < fileCheck.size() - 1; i++) {

			int former = Integer.parseInt(fileCheck.get(i).substring(0, 8));
			int latter = Integer.parseInt(fileCheck.get(i + 1).substring(0, 8));

			if ((latter - former) != 1) {
				System.out.println(FILE_NOT_SERIAL_NUMBER);
				return;
			}
		}

		//rcdFilesを読み込んでリスト「salesResults」に格納しMAP参照に格納
		for (int i = 0; i < rcdFiles.size(); i++) {
			BufferedReader br = null;
			String line;
			try {
				FileReader fr = new FileReader(rcdFiles.get(i));
				List<String> salesResults = new ArrayList<>();
				br = new BufferedReader(fr);
				while ((line = br.readLine()) != null) {
					salesResults.add(line);
				}
				if (salesResults.size() != 3) {
					System.out.println(rcdFiles.get(i).getName() + FILE_FORMAT_ERROR);
					return;
				}
				if (!branchNames.containsKey(salesResults.get(0))) {
					System.out.println(rcdFiles.get(i).getName() + FILE_BRANCH_NAME_ERROR);
					return;
				}
				if (!salesResults.get(2).matches("[0-9]{1,}")) {
					System.out.println(UNKNOWN_ERROR);
					return;
				}
				long fileSale = Long.parseLong(salesResults.get(2));

				Long saleAmount = fileSale = branchSales.get(salesResults.get(0)) + fileSale;
				saleAmount = fileSale = commoditySales.get(salesResults.get(1)) + fileSale;
				if (saleAmount >= 1000000000L) {
					System.out.println(FILE_TOTAL_AMOUNT_OVER);
					return;
				}

				branchSales.put(salesResults.get(0), fileSale);

				fileSale = Long.parseLong(salesResults.get(2));
				saleAmount = fileSale = commoditySales.get(salesResults.get(1)) + fileSale;
				if (saleAmount >= 1000000000L) {
					System.out.println(FILE_TOTAL_AMOUNT_OVER);
					return;
				}

				commoditySales.put(salesResults.get(1), fileSale);

			} catch (IOException e) {
				System.out.println(UNKNOWN_ERROR);
				return;
			} finally {
				// ファイルを開いている場合
				if (br != null) {
					try {
						// ファイルを閉じる
						br.close();
					} catch (IOException e) {
						System.out.println(UNKNOWN_ERROR);
						return;
					}
				}
			}
		}

		// 支店別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_BRANCH_OUT, branchNames, branchSales)) {
			return;
		}

		// 商品別集計ファイル書き込み処理
		if (!writeFile(args[0], FILE_NAME_COMMODITY_OUT, commodityNames, commoditySales)) {
			return;
		}

	}

	/**
	 * 支店定義ファイル読み込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 読み込み可否
	 */
	private static boolean readFile(String path, String fileName, Map<String, String> nameMap,
			Map<String, Long> saleMap, String rule, String name) {
		BufferedReader br = null;

		try {
			File file = new File(path, fileName);
			if (!file.exists()) {
				System.out.println(name + FILE_NOT_EXIST);
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			// 一行ずつ読み込む
			while ((line = br.readLine()) != null) {
				// ※ここの読み込み処理を変更してください。(処理内容1-2)
				String[] branch = line.split(",");
				if ((branch.length != 2) || (!branch[0].matches(rule))) {
					System.out.println(name + FILE_INVALID_FORMAT);
					return false;
				}

				nameMap.put(branch[0], branch[1]);
				saleMap.put(branch[0], (long) 0);

			}

		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			// ファイルを開いている場合
			if (br != null) {
				try {
					// ファイルを閉じる
					br.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

	/**
	 * 支店別集計ファイル書き込み処理
	 *
	 * @param フォルダパス
	 * @param ファイル名
	 * @param 支店コードと支店名を保持するMap
	 * @param 支店コードと売上金額を保持するMap
	 * @return 書き込み可否
	 */
	private static boolean writeFile(String path, String fileName, Map<String, String> nameMap,
			Map<String, Long> saleMap) {
		// ※ここに書き込み処理を作成してください。(処理内容3-1)
		BufferedWriter bw = null;

		try {
			File file = new File(path, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			for (String key : nameMap.keySet()) {
				bw.write(key + "," + nameMap.get(key) + "," + saleMap.get(key));
				bw.newLine();
			}
		} catch (IOException e) {
			System.out.println(UNKNOWN_ERROR);
			return false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println(UNKNOWN_ERROR);
					return false;
				}
			}
		}
		return true;
	}

}
